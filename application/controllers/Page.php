<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Page extends CI_Controller
{

    public function __construct()
    {

        parent::__construct();

    }

    /**
     * Initialisation de la vue avec la liste des catégories
     *
     * @return Response
     */
    public function index()
    {
        $data['page_title'] = 'Cas pratique Allovoisins';

        $data['categories'] = $this->Categories_model->getCategories();
        $listObjects = $this->Objects_model->getListName();

        $list = array();

        if ($listObjects) {
            foreach ($listObjects as $l) {
                array_push($list, $l["name"]);
            }
        }

        $data['objecstName'] = json_encode($list);

        $this->load->view('template/header', $data);
        $this->load->view('template/nav');
        $this->load->view('liste');
        $this->load->view('template/footer');
    }

    /**
     * Retourne les objets par rapport aux filtres et à la pagination
     *
     * @return Response
     */

    public function loadObjects()
    {

        $pageLimit = 30;
        $limit = (int) $this->uri->segment(3) > 0 ? ($this->uri->segment(3) - 1) * $pageLimit : 0;

        $inputObject = $this->input->post("inputObject");
        $inputCategObject = $this->input->post("inputCategObject");
        $inputDateObject = $this->input->post("inputDateObject");
        $inputSortObject = $this->input->post("inputSortObject");

        $formatDateObject = "";

        if ($inputDateObject != "") {
            $formatDateObject = date('Y-m-d', strtotime($inputDateObject));
        }

        $filters = array("limit" => $limit,
            "pageLimit" => $pageLimit,
            "objectName" => $inputObject,
            "categObject" => $inputCategObject,
            "dateObject" => $formatDateObject,
            "sortObject" => $inputSortObject,
        );

        $objectsResult = $this->Objects_model->getObjects($filters);
        $totalObjectsResult = $this->Objects_model->getTotalObjects($filters);

        $this->load->library('pagination');

        $config['base_url'] = base_url() . 'index.php/page/loadObjects';
        $config['use_page_numbers'] = true;
        $config['total_rows'] = $totalObjectsResult;
        $config['per_page'] = $pageLimit;

        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tag_close'] = '<span aria-hidden="true"></span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tag_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tag_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tag_close'] = '</span></li>';

        $this->pagination->initialize($config);

        $data['count'] = $totalObjectsResult;
        $data['pagination'] = $this->pagination->create_links();
        $data['result'] = $objectsResult;
        $data['filters'] = $filters;

        echo json_encode($data);

    }
}
