<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Categories_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Retourne toutes les catégories
     *
     * @return Response
     */
    public function getCategories()
    {

        $this->db->select('*');
        $this->db->from('categories');
        $query = $this->db->get();

        return $query->result_array();
    }

}
