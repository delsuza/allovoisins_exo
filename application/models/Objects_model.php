<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Objects_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Retourne tous les nom d'objets
     *
     * @return Response
     */
    public function getListName()
    {

        $this->db->select('objects.name');
        $this->db->from('objects');

        $query = $this->db->get();

        return $query->result_array();
    }

    /**
     * Retourne tous les objets filtrés
     *
     * @return Response
     */
    public function getObjects($filters)
    {

        $this->db->select('objects.name AS object_name, categories.name AS categorie_name, created_at');
        $this->db->from('objects');
        $this->db->join('categories', 'objects.category_id = categories.category_id');

        if ($filters["categObject"] > 0) {
            $this->db->where('objects.category_id', $filters["categObject"]);
        }

        if ($filters["objectName"] != "") {
            $this->db->like('objects.name', $filters["objectName"]);
        }

        if ($filters["dateObject"] != "") {
            $this->db->where('objects.created_at >', "'" . $filters["dateObject"] . " 00:00:00'", false);
            $this->db->where('objects.created_at <', "'" . $filters["dateObject"] . " 23:59:59'", false);
        }

        if ($filters["sortObject"]) {
            $this->db->order_by('objects.name', 'DESC');
        } else {
            $this->db->order_by('objects.name', 'ASC');
        }

        $this->db->limit($filters["pageLimit"], $filters["limit"]);

        $query = $this->db->get();

        return $query->result_array();
    }

    /**
     *  Retourne le nombre d'objets filtrés
     *
     * @return Response
     */
    public function getTotalObjects($filters)
    {

        $this->db->select('count(*) as total');
        $this->db->from('objects');

        if ($filters["categObject"] > 0) {
            $this->db->where('objects.category_id', $filters["categObject"]);
        }

        if ($filters["objectName"] != "") {
            $this->db->like('objects.name', $filters["objectName"]);
        }

        if ($filters["dateObject"] != "") {
            $this->db->where('objects.created_at', $filters["dateObject"]);
        }

        if ($filters["sortObject"]) {
            $this->db->order_by('objects.name', 'DESC');
        } else {
            $this->db->order_by('objects.name', 'ASC');
        }

        $query = $this->db->get();
        $result = $query->result_array();

        return $result[0]['total'];
    }

}
