

 <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://getbootstrap.com/docs/4.0/assets/js/vendor/popper.min.js"></script>
    <script src="https://getbootstrap.com/docs/4.0/dist/js/bootstrap.min.js"></script>
    <!-- Datepicker -->
    <script src="https://unpkg.com/gijgo@1.9.11/js/gijgo.min.js" type="text/javascript"></script>
    <!-- Icons -->
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
     <!-- Typeahead -->
    <script src="<?=base_url()?>assets/js/typeahead.bundle.min.js"></script>

    <script>
      feather.replace()
    </script>

    <!-- Data table -->
    <script type='text/javascript'>
   $(document).ready(function(){

     // Detect pagination click
     $('#pagination').on('click','a',function(e){
       e.preventDefault(); 
       var pageno = $(this).attr('data-ci-pagination-page');
       loadPagination(pageno);
     });

     $('#inputCategObject').on('change',function(e){
       e.preventDefault(); 
       var pageno = parseInt($('#pagination li .active').text());
       loadPagination(pageno);
     });

     $('#inputSortObject').on('change',function(e){
       e.preventDefault(); 
       var pageno = parseInt($('#pagination li .active').text());
       loadPagination(pageno);
     });

    var $datepicker = $('#inputDateObject').datepicker({
            uiLibrary: 'bootstrap4',
            format: 'dd-mm-yyyy',
            change: function(e) {
              loadPagination(0);
            }

      });

     loadPagination(0);

     // Load pagination
     function loadPagination(pagno){
       var inputObject = $("#inputObject").val();
       var inputCategObject = $("#inputCategObject").val();
       var inputDateObject = $datepicker.value();
       var inputSortObject = $("#inputSortObject").val();

      $.ajax({
         url: '<?=base_url()?>index.php/page/loadObjects/'+pagno,
         type: 'post',
         data:{"inputObject": inputObject, 
               "inputCategObject": inputCategObject, 
               "inputDateObject": inputDateObject, 
               "inputSortObject": inputSortObject,
               },
         dataType: 'json',
         success: function(response){
            $('#pagination').html(response.pagination);
            $('#count').html(response.count+" objects");
            createTable(response.result);
         }
       });
     }

     // Create table list
     function createTable(result){
       $('.table-data tbody').empty();
       for(index in result){
          var tr = "<tr>";
          tr += "<td>"+ result[index].object_name +"</td>";
          tr += "<td>"+ result[index].categorie_name +"</a></td>";
          tr += "<td>"+ result[index].created_at +"</td>";
          tr += "</tr>";
          $('tbody').append(tr);
 
        }
      }

      var listName = <?= $objecstName ?>;

      // constructs the suggestion engine
      var query = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        // `states` is an array of state names defined in "The Basics"
        local: listName
      });

      $('#bloodhound .typeahead').typeahead({
        hint: true,
        highlight: true,
        minLength: 1
      },
      {
        name: 'objets',
        source: query
      });

      $("#bloodhound .typeahead").keyup(function() {
        if (!this.value) {
            loadPagination(0);
        }});

      $('#bloodhound .typeahead').on('typeahead:select', function(evt, item) {
          loadPagination(0);
      });
    });
    </script>
  </body>
</html>