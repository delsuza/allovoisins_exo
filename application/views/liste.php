<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
            
            <div class="btn-toolbar mb-2 mb-md-0">
              <div class="btn-group mr-2">
                  <div id="bloodhound">
	              	    <input type="text" class="form-control typeahead" id="inputObject"  placeholder="Rechercher un objet">
                 </div>
	               
	                <select class="form-control" id="inputCategObject">
                  <option value="-1">Toutes les catégories</option>
	                	<?php
	                		if($categories){
	                			foreach ($categories as $c) {
	                				echo "<option value='".$c['category_id']."'>".$c['name']."</option>";
	                			}
	                		}
	                	 ?>
					  
					</select>
				</div>
				<div class="btn-group mr-2">

                  <input id="inputDateObject" width="276" />

                  <select class="form-control" id="inputSortObject">
                  	<option value="0">Croissant</option>
                  	<option value="1">Décroissant</option>
                  </select>
              </div>
              
            </div>
          </div>

          <div class="table-responsive">
            <table class="table table-striped table-sm table-data">
              <thead>
                <tr>
                  <th>Objet</th>
                  <th>Catégorie</th>
                  <th>Date</th>
                </tr>
              </thead>
              <tbody>
                
              </tbody>
            </table>
            <div id='pagination'></div>
            <div id='count'></div>
          </div>
        </main>
      </div>
    </div>