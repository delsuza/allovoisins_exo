**EXO 1 :**

- On sélectionne que le champ "cle" afin d'optimiser la requête car la fonction retourne que la valeur de "cle".<br/>
- Si user_id est un identifiant unique, on peut rechercher les infos d'un utilisateur que par son id.<br/>
- Une erreur sur la ligne $this->slaveDB->select($selec); >> mauvais nom de variable<br/>
- $resultsql retourne un tableau d'objet >> Array ( [0] => stdClass Object ( [cle] => 2232323 ) ), il faut selectionner le premier index du tableau pour avoir le résultat<br>


```php
public function cleDeSecurite($idu){

$select = array(

'cle'

);

$this->slaveDB->select($select);

$this->slaveDB->from('securite');

$this->slaveDB->where_in('user_id', $idu);

$resultsql = $this->slaveDB->get()->result();

return $resultsql[0]->cle;

} 
```

**EXO 2 :**

```css

/* la largeur sera calculé par le navigateur sur la classe container dans les éléments ayant un id main_container, topbar et header */
#main_container .container, #topbar .container, #header .container {

width : auto;

}
/* la largeur est définie à 100% sur la classe bootstrap col-xs-6 dans l'élément ayant un id signup-form */
#signup-form .col-xs-6 {

width:100%;

}

/* seuls les éléments div enfants de categories-lists sont ciblés, le 3ème enfants en première position aura une bordure top de 1px et de couleur DCDCDC 
  */ 
#categories-lists>div>div>div:first-of-type {

/*border-top:none;*/

border-top:1px solid #DCDCDC ;

}

/* le label ayant comme parents category-bloc categories-lists et new alert aura une zone de remplissage intérieur en haut de 0px.
Le curseur de la souris sera de forme pointeur, un type d'affichage intérieur de type table, une hauteur de 50px, un dépassement d'affichage rogné,
une largeur de 90%, une marge extérieur à gauche de -50px, une zone de remplissage intérieur à gauche de 50px, l'élélement sera flottant du coté gauche du flux
*/
#new-alert #categories-lists .category-bloc label {

padding-top: 0 !important;

cursor: pointer;

display: table;

height: 50px;

overflow: hidden;

width: 90%;/*100%*/

margin-left: -50px !important;

padding-left: 50px !important;

float: left;

}
```

**EXO 3 :**

```sql
SELECT u.user_id, 
u.first_name, 
u.last_name, 
u.email_address, 
u.created_at,

(SELECT count(*) AS total 
FROM searches s
WHERE s.user_id = u.user_id
AND s.status >= -1) 
AS nb_searches,

(SELECT count(*) AS total 
FROM offers o
INNER JOIN searches s ON s.search_id = o.search_id
WHERE o.user_id = u.user_id
AND o.status >= 3
AND s.status >= -1
) AS nb_accepted_offers,

(SELECT count(*) AS total 
FROM offers o
INNER JOIN searches s ON s.search_id = o.search_id
WHERE o.user_id = u.user_id
AND o.status = 5
AND s.status >= -1
) AS nb_accepted_offers_offline,

(SELECT count(*) AS total 
FROM offers o
INNER JOIN searches s ON s.search_id = o.search_id
WHERE o.user_id = u.user_id
AND o.status != 5
AND o.status >= 3
AND s.status >= -1
) AS nb_accepted_offers_online

FROM users u
INNER JOIN addresses a ON u.user_id = a.user_id
INNER JOIN cities c ON a.city_id = c.city_id
WHERE c.country = "FR"
AND a.is_default = 1
GROUP BY u.user_id
HAVING nb_accepted_offers >=1
```

**CAS PRATIQUE :** <br>
[DEMO ICI](http://134.209.27.49/allovoisins_exo/)<br>
Le fichier config/database.php est à configurer. <br>
Temps estimé :<br>
Installation environnement VPS : 30mn<br>
PHP : 2h30<br>
JS : 1h<br>
CSS : 15mn<br>
HTML : 30mn